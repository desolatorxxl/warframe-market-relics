package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"
)

var relics map[string]*Relic

type Relics map[string]*Relic

type DroppableItem struct {
	Name   string
	Price  int
	ID     string
	Chance float64
}

type Relic struct {
	Name  string
	Score float64
	Drops []DroppableItem
}

func (r *Relic) AddDrop(drop DroppableItem) {
	r.Drops = append(r.Drops, drop)
}

func main() {
	relics = make(map[string]*Relic)

	items, err := readJSON()
	if err != nil {
		log.Fatal(err)
	}

	for _, item := range items {
		if len(item.Components) > 0 {
			for _, component := range item.Components {
				if component.Ducats > 0 {
					println(item.Name, component.Name)
					orders, err := getOrders(item.Name + " " + component.Name)
					if err != nil {
						fmt.Println(err)
					}

					for _, drop := range component.Drops {
						if len(orders) > 0 {
							item := DroppableItem{
								ID:     drop.ID(),
								Name:   drop.Type,
								Chance: drop.Chance,
								Price:  orders[0].Platinum,
							}
							if _, ok := relics[drop.Location]; ok {
								relics[drop.Location].AddDrop(item)
							} else {
								relics[drop.Location] = &Relic{
									Name:  drop.Location,
									Drops: []DroppableItem{item},
								}
							}
						}
					}
				}
			}
		}
	}

	var oof []*Relic
	for _, relic := range relics {
		var score float64
		for _, drop := range relic.Drops {
			score += drop.Chance * float64(drop.Price)
		}
		relic.Score = score
		oof = append(oof, relic)
	}
	sort.SliceStable(oof, func(i, j int) bool {
		return oof[i].Score < oof[j].Score
	})
	for _, score := range oof {
		if strings.Contains(score.Name, "Radiant") {
			fmt.Printf("%s %.2f\n", score.Name, score.Score)
			for _, item := range score.Drops {
				fmt.Printf("\t%dp\t%s (%.2f%%)\n", item.Price, item.Name, item.Chance*100)
			}
		}
	}
	println("Done")
}
func getOrdersFile(file string) ([]byte, error) {
	bodyBytes, err := os.ReadFile(file)
	if err != nil {
		return nil, fmt.Errorf("failed to read file %s request: %w", file, err)
	}
	return bodyBytes, err
}

func getOrdersHTTP(i string) ([]byte, error) {
	time.Sleep(time.Millisecond * 500)
	client := &http.Client{
		Timeout: time.Second * 5,
	}

	req, err := http.NewRequestWithContext(context.Background(), "GET", fmt.Sprintf("https://api.warframe.market/v1/items/%s/orders", i), nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create GET request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to send GET request %s: %w", resp.Status, err)
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %w", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to get warframe market orders, status code %d", resp.StatusCode)
	}

	err = resp.Body.Close()
	if err != nil {
		return nil, fmt.Errorf("failed to close response body: %w", err)
	}

	err = os.WriteFile(fmt.Sprintf("%s.json", i), bodyBytes, 0644)
	if err != nil {
		return nil, fmt.Errorf("failed to write to %s.json: %w", i, err)
	}
	return bodyBytes, nil
}

func getOrders(i string) ([]WarframeMarketOrder, error) {
	var bodyBytes []byte

	i = strings.ToLower(i)
	i = strings.ReplaceAll(i, " ", "_")
	i = strings.ReplaceAll(i, "&", "and")

	file := fmt.Sprintf("%s.json", i)

	if _, err := os.Stat(file); err == nil {
		bodyBytes, err = getOrdersFile(file)
		if err != nil {
			return nil, fmt.Errorf("error getting orders from file: %w", err)
		}
	} else {
		bodyBytes, err = getOrdersHTTP(i)
		if err != nil {
			return nil, fmt.Errorf("error getting orders from warframe market: %w", err)
		}
	}

	data := WarframeMarketResponse{}

	err := json.Unmarshal(bodyBytes, &data)
	if err != nil {
		return nil, fmt.Errorf("error parsing item JSON to struct: %w", err)
	}

	orders := []WarframeMarketOrder{}

	for _, order := range data.Payload.Orders {
		if order.OrderType == "sell" && order.User.Status == "ingame" {
			orders = append(orders, order)
		}
	}

	sort.SliceStable(orders, func(i, j int) bool {
		return orders[i].Platinum < orders[j].Platinum
	})

	return orders, nil
}

func (g *GitHubComponent) ID() string {
	i := strings.ToLower(g.Name)
	i = strings.ReplaceAll(i, " ", "_")
	i = strings.ReplaceAll(i, "&", "and")
	return i
}

func (g *GitHubDrop) ID() string {
	i := strings.ToLower(g.Type)
	i = strings.ReplaceAll(i, " ", "_")
	i = strings.ReplaceAll(i, "&", "and")
	return i
}

func readJSON() (GitHubItems, error) {
	file, err := ioutil.ReadFile("All.json")
	if err != nil {
		return nil, fmt.Errorf("error reading item JSON: %w", err)
	}

	data := GitHubItems{}

	err = json.Unmarshal([]byte(file), &data)
	if err != nil {
		return nil, fmt.Errorf("error parsing item JSON to struct: %w", err)
	}

	return data, nil
}
