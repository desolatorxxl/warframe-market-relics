package main

import "time"

type GitHubItems []GitHubItem

type GitHubItem struct {
	Accuracy float64 `json:"accuracy"`
	Ammo     int     `json:"ammo"`
	Attacks  []struct {
		Name         string  `json:"name"`
		Speed        float64 `json:"speed"`
		CritChance   float64 `json:"crit_chance"`
		CritMult     float64 `json:"crit_mult"`
		StatusChance float64 `json:"status_chance"`
		ShotType     string  `json:"shot_type"`
		ShotSpeed    int     `json:"shot_speed"`
		Damage       struct {
			Impact   float64 `json:"impact"`
			Slash    float64 `json:"slash"`
			Puncture float64 `json:"puncture"`
		} `json:"damage"`
	} `json:"attacks"`
	BuildPrice         int               `json:"buildPrice"`
	BuildQuantity      int               `json:"buildQuantity"`
	BuildTime          int               `json:"buildTime"`
	Category           string            `json:"category"`
	Components         []GitHubComponent `json:"components"`
	ConsumeOnBuild     bool              `json:"consumeOnBuild"`
	CriticalChance     float64           `json:"criticalChance"`
	CriticalMultiplier float64           `json:"criticalMultiplier"`
	DamagePerShot      []float64         `json:"damagePerShot"`
	Description        string            `json:"description"`
	Disposition        int               `json:"disposition"`
	EstimatedVaultDate string            `json:"estimatedVaultDate"`
	FireRate           float64           `json:"fireRate"`
	ImageName          string            `json:"imageName"`
	Introduced         struct {
		Name    string   `json:"name"`
		URL     string   `json:"url"`
		Aliases []string `json:"aliases"`
		Parent  string   `json:"parent"`
		Date    string   `json:"date"`
	} `json:"introduced"`
	MagazineSize     int     `json:"magazineSize"`
	MasteryReq       int     `json:"masteryReq"`
	Multishot        int     `json:"multishot"`
	Name             string  `json:"name"`
	Noise            string  `json:"noise"`
	OmegaAttenuation float64 `json:"omegaAttenuation"`
	Patchlogs        []struct {
		Name      string    `json:"name"`
		Date      time.Time `json:"date"`
		URL       string    `json:"url"`
		Additions string    `json:"additions"`
		Changes   string    `json:"changes"`
		Fixes     string    `json:"fixes"`
	} `json:"patchlogs"`
	Polarities         []string `json:"polarities"`
	ProcChance         float64  `json:"procChance"`
	ProductCategory    string   `json:"productCategory"`
	ReleaseDate        string   `json:"releaseDate"`
	ReloadTime         float64  `json:"reloadTime"`
	SkipBuildTimePrice int      `json:"skipBuildTimePrice"`
	Slot               int      `json:"slot"`
	Tags               []string `json:"tags"`
	TotalDamage        float64  `json:"totalDamage"`
	Tradable           bool     `json:"tradable"`
	Trigger            string   `json:"trigger"`
	Type               string   `json:"type"`
	UniqueName         string   `json:"uniqueName"`
	VaultDate          string   `json:"vaultDate"`
	Vaulted            bool     `json:"vaulted"`
	WikiaThumbnail     string   `json:"wikiaThumbnail"`
	WikiaURL           string   `json:"wikiaUrl"`
}

type GitHubComponent struct {
	UniqueName        string       `json:"uniqueName"`
	Name              string       `json:"name"`
	Description       string       `json:"description"`
	PrimeSellingPrice int          `json:"primeSellingPrice"`
	ItemCount         int          `json:"itemCount"`
	ImageName         string       `json:"imageName"`
	Tradable          bool         `json:"tradable"`
	Drops             []GitHubDrop `json:"drops"`
	Ducats            int          `json:"ducats"`
}

type GitHubDrop struct {
	Chance   float64 `json:"chance"`
	Location string  `json:"location"`
	Rarity   string  `json:"rarity"`
	Type     string  `json:"type"`
}

type WarframeMarketResponse struct {
	Payload struct {
		Orders []WarframeMarketOrder `json:"orders"`
	} `json:"payload"`
}

type WarframeMarketOrder struct {
	OrderType string `json:"order_type"`
	Quantity  int    `json:"quantity"`
	Platinum  int    `json:"platinum"`
	Visible   bool   `json:"visible"`
	User      struct {
		Reputation int         `json:"reputation"`
		Region     string      `json:"region"`
		Avatar     interface{} `json:"avatar"`
		LastSeen   time.Time   `json:"last_seen"`
		IngameName string      `json:"ingame_name"`
		Status     string      `json:"status"`
		ID         string      `json:"id"`
	} `json:"user"`
	Platform     string    `json:"platform"`
	Region       string    `json:"region"`
	CreationDate time.Time `json:"creation_date"`
	LastUpdate   time.Time `json:"last_update"`
	ID           string    `json:"id"`
}
